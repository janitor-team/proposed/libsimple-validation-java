#!/bin/sh -e
# $1 = version

TAR=../libsimple-validation-java_$1.orig.tar.gz
DIR=libsimple-validation-java-$1.orig
ZIP=src.zip
URL=http://kenai.com/projects/simplevalidation/downloads/download/

wget $URL/$ZIP

# clean up the upstream tarball
mkdir $DIR
(cd $DIR; unzip ../$ZIP)
GZIP=--best tar -c -z -f $TAR  --exclude '*.jar' $DIR
rm -rf $DIR $ZIP

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
